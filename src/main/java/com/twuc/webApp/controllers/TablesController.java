package com.twuc.webApp.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.InvalidParameterException;

@RestController
@RequestMapping("/api/tables")
public class TablesController {
    @GetMapping("/plus")
    public String getTableOfPlus() {
        return calculate("+");
    }

    @GetMapping("/multiply")
    public String getTableOfMultiply() {
        return calculate("*");
    }

    private String calculate(String operation) {
        StringBuilder result = new StringBuilder();
        result.append("<pre>");
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                int res = operation.equals("+") ? i + j : i * j;
                String format = String.format("%d" + operation + "%d=%d", i, j, res);
                result.append(format);
                if (i != j) {
                    result.append(res >= 10 ? " " : "  ");
                }
            }
            result.append("\n");
        }
        result.append("</pre>");
        return String.valueOf(result);
    }
}
